package pl.bartoszwesolowski.musicplaces.logic

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import pl.bartoszwesolowski.musicplaces.model.Place
import java.io.IOException
import java.util.concurrent.TimeUnit

class MapViewModelTest {

    @Test
    fun `should the initial state be MapNotReady`() {
        // given
        val viewModel = MapViewModel(mock(), Schedulers.trampoline(), Schedulers.trampoline())
        val viewStateObserver = TestObserver<MapViewState>()

        // when
        viewModel.viewState.subscribe(viewStateObserver)

        // then
        viewStateObserver.assertValues(MapViewState.MapNotReady)
    }

    @Test
    fun `should state be NoQuery when map is ready`() {
        // given
        val viewModel = MapViewModel(mock(), Schedulers.trampoline(), Schedulers.trampoline())
        val viewStateObserver = TestObserver<MapViewState>()

        // when
        viewModel.viewState.subscribe(viewStateObserver)
        viewModel.mapReady = true

        // then
        viewStateObserver.assertValues(
            MapViewState.MapNotReady,
            MapViewState.NoQuery
        )
    }

    @Test
    fun `should not show places when map not ready`() {
        // given
        val placesProvider: PlacesProvider = mock {
            on { getPlaces(QUERY) } doReturn Single.just(listOf(Place("id", "name", 1.0, 1.0, 1991)))
        }
        val viewModel = MapViewModel(placesProvider, Schedulers.trampoline(), Schedulers.trampoline())
        val viewStateObserver = TestObserver<MapViewState>()

        // when
        viewModel.viewState.subscribe(viewStateObserver)
        viewModel.findPlaces(QUERY)

        // then
        viewStateObserver.assertValues(
            MapViewState.MapNotReady
        )
    }

    @Test
    fun `should show progress when fetching places`() {
        // given
        val placesProvider: PlacesProvider = mock {
            on { getPlaces(QUERY) } doReturn Single.never()
        }
        val viewModel = MapViewModel(placesProvider, Schedulers.trampoline(), Schedulers.trampoline())
        val viewStateObserver = TestObserver<MapViewState>()

        // when
        viewModel.viewState.subscribe(viewStateObserver)
        viewModel.mapReady = true
        viewModel.findPlaces(QUERY)

        // then
        viewStateObserver.assertValues(
            MapViewState.MapNotReady,
            MapViewState.NoQuery,
            MapViewState.InProgress(QUERY)
        )
    }

    @Test
    fun `should show places when they are fetched and map is ready`() {
        // given
        val places = listOf(Place("id", "name", 1.0, 1.0, 1991))
        val placesProvider: PlacesProvider = mock {
            on { getPlaces(QUERY) } doReturn Single.just(places)
        }
        val viewModel = MapViewModel(placesProvider, Schedulers.trampoline(), Schedulers.trampoline())
        val viewStateObserver = TestObserver<MapViewState>()

        // when
        viewModel.viewState.subscribe(viewStateObserver)
        viewModel.mapReady = true
        viewModel.findPlaces(QUERY)

        // then
        viewStateObserver.assertValues(
            MapViewState.MapNotReady,
            MapViewState.NoQuery,
            MapViewState.InProgress(QUERY),
            MapViewState.ShowPlaces(QUERY, places, true)
        )
    }

    @Test
    fun `should show no results info when places fetched are empty`() {
        // given
        val places = emptyList<Place>()
        val placesProvider: PlacesProvider = mock {
            on { getPlaces(QUERY) } doReturn Single.just(places)
        }
        val viewModel = MapViewModel(placesProvider, Schedulers.trampoline(), Schedulers.trampoline())
        val viewStateObserver = TestObserver<MapViewState>()

        // when
        viewModel.viewState.subscribe(viewStateObserver)
        viewModel.mapReady = true
        viewModel.findPlaces(QUERY)

        // then
        viewStateObserver.assertValues(
            MapViewState.MapNotReady,
            MapViewState.NoQuery,
            MapViewState.InProgress(QUERY),
            MapViewState.NoResults(QUERY)
        )
    }

    @Test
    fun `should show error when fetching places failed`() {
        // given
        val placesProvider: PlacesProvider = mock {
            on { getPlaces(QUERY) } doReturn Single.error(IOException())
        }
        val viewModel = MapViewModel(placesProvider, Schedulers.trampoline(), Schedulers.trampoline())
        val viewStateObserver = TestObserver<MapViewState>()

        // when
        viewModel.viewState.subscribe(viewStateObserver)
        viewModel.mapReady = true
        viewModel.findPlaces(QUERY)

        // then
        viewStateObserver.assertValues(
            MapViewState.MapNotReady,
            MapViewState.NoQuery,
            MapViewState.InProgress(QUERY),
            MapViewState.PlacesFetchingError(QUERY)
        )
    }

    @Test
    fun `should hide places when map is destroyed`() {
        // given
        val places = listOf(Place("id", "name", 1.0, 1.0, 1991))
        val placesProvider: PlacesProvider = mock {
            on { getPlaces(QUERY) } doReturn Single.just(places)
        }
        val viewModel = MapViewModel(placesProvider, Schedulers.trampoline(), Schedulers.trampoline())
        val viewStateObserver = TestObserver<MapViewState>()

        // when
        viewModel.viewState.subscribe(viewStateObserver)
        viewModel.mapReady = true
        viewModel.findPlaces(QUERY)
        viewModel.mapReady = false

        // then
        viewStateObserver.assertValues(
            MapViewState.MapNotReady,
            MapViewState.NoQuery,
            MapViewState.InProgress(QUERY),
            MapViewState.ShowPlaces(QUERY, places, true),
            MapViewState.MapNotReady
        )
    }

    @Test
    fun `should show hide places when they become out of date`() {
        // given
        val place1 = Place("id1", "name1", 1.0, 1.0, 1991)
        val place2 = Place("id2", "name2", 2.0, 2.0, 1992)
        val places = listOf(place1, place2)
        val placesProvider: PlacesProvider = mock {
            on { getPlaces(QUERY) } doReturn Single.just(places)
        }
        val testScheduler = TestScheduler()
        val viewModel = MapViewModel(placesProvider, Schedulers.trampoline(), Schedulers.trampoline(), testScheduler)
        val viewStateObserver = TestObserver<MapViewState>()

        // when
        viewModel.viewState.subscribe(viewStateObserver)
        viewModel.mapReady = true
        viewModel.findPlaces(QUERY)

        // then
        viewStateObserver.assertValues(
            MapViewState.MapNotReady,
            MapViewState.NoQuery,
            MapViewState.InProgress(QUERY),
            MapViewState.ShowPlaces(QUERY, places, true)
        )

        // when
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)

        // then
        viewStateObserver.assertValues(
            MapViewState.MapNotReady,
            MapViewState.NoQuery,
            MapViewState.InProgress(QUERY),
            MapViewState.ShowPlaces(QUERY, places, true),
            MapViewState.ShowPlaces(QUERY, listOf(place2), false)
        )

        // when
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)

        // then
        viewStateObserver.assertValues(
            MapViewState.MapNotReady,
            MapViewState.NoQuery,
            MapViewState.InProgress(QUERY),
            MapViewState.ShowPlaces(QUERY, places, true),
            MapViewState.ShowPlaces(QUERY, listOf(place2), false),
            MapViewState.ShowPlaces(QUERY, emptyList(), false)
        )
    }

    companion object {
        private const val QUERY = "studios"
    }
}