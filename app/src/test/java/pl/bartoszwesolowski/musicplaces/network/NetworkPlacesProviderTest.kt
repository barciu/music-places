package pl.bartoszwesolowski.musicplaces.network

import com.nhaarman.mockitokotlin2.*
import io.reactivex.observers.TestObserver
import okhttp3.ResponseBody
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import pl.bartoszwesolowski.musicplaces.model.Place
import pl.bartoszwesolowski.musicplaces.network.dto.CoordinatesDto
import pl.bartoszwesolowski.musicplaces.network.dto.LifespanDto
import pl.bartoszwesolowski.musicplaces.network.dto.PlaceDto
import pl.bartoszwesolowski.musicplaces.network.dto.PlacesResponse
import retrofit2.Call
import retrofit2.HttpException
import retrofit2.Response

class NetworkPlacesProviderTest {

    @Test
    fun `should fetch single page`() {
        // given
        val limit = 2
        val page1Call = mockSuccessfulCall(
            1,
            0,
            listOf(
                PlaceDto("id1", "name1", CoordinatesDto(1.0, 1.0), LifespanDto("1991"))
            )
        )
        val musicBrainz: MusicBrainzInterface = mock {
            on { getPlaces(anyString(), eq(limit), eq(0)) } doReturn page1Call
        }
        val placesProvider = NetworkPlacesProvider(limit, musicBrainz)
        val testObserver = TestObserver<List<Place>>()

        // when
        placesProvider.getPlaces("studios").subscribe(testObserver)

        // then
        verify(musicBrainz).getPlaces(anyString(), eq(limit), eq(0))
        verifyNoMoreInteractions(musicBrainz)

        testObserver.assertComplete()
        testObserver.assertValue(
            listOf(
                Place("id1", "name1", 1.0, 1.0, 1991)
            )
        )
    }

    @Test
    fun `should fetch multiple pages`() {
        // given
        val limit = 2
        val page1Call = mockSuccessfulCall(
            4,
            0,
            listOf(
                PlaceDto("id1", "name1", CoordinatesDto(1.0, 1.0), LifespanDto("1991")),
                PlaceDto("id2", "name2", CoordinatesDto(2.0, 2.0), LifespanDto("1992"))
            )
        )
        val page2Call = mockSuccessfulCall(
            4,
            2,
            listOf(
                PlaceDto("id3", "name3", CoordinatesDto(3.0, 3.0), LifespanDto("1993")),
                PlaceDto("id4", "name4", CoordinatesDto(4.0, 4.0), LifespanDto("1994"))
            )
        )
        val musicBrainz: MusicBrainzInterface = mock {
            on { getPlaces(anyString(), eq(limit), eq(0)) } doReturn page1Call
            on { getPlaces(anyString(), eq(limit), eq(2)) } doReturn page2Call
        }
        val placesProvider = NetworkPlacesProvider(limit, musicBrainz)
        val testObserver = TestObserver<List<Place>>()

        // when
        placesProvider.getPlaces("studios").subscribe(testObserver)

        // then
        verify(musicBrainz).getPlaces(anyString(), eq(limit), eq(0))
        verify(musicBrainz).getPlaces(anyString(), eq(limit), eq(2))
        verifyNoMoreInteractions(musicBrainz)

        testObserver.assertComplete()
        testObserver.assertValue(
            listOf(
                Place("id1", "name1", 1.0, 1.0, 1991),
                Place("id2", "name2", 2.0, 2.0, 1992),
                Place("id3", "name3", 3.0, 3.0, 1993),
                Place("id4", "name4", 4.0, 4.0, 1994)
            )
        )
    }

    @Test
    fun `should report single page error`() {
        // given
        val limit = 2
        val page1Call: Call<PlacesResponse> = mockErrorCall()
        val musicBrainz: MusicBrainzInterface = mock {
            on { getPlaces(anyString(), eq(limit), eq(0)) } doReturn page1Call
        }
        val placesProvider = NetworkPlacesProvider(limit, musicBrainz)
        val testObserver = TestObserver<List<Place>>()

        // when
        placesProvider.getPlaces("studios").subscribe(testObserver)

        // then
        testObserver.assertNoValues()
        testObserver.assertError(HttpException::class.java)
    }

    @Test
    fun `report second page error`() {
        // given
        val limit = 2
        val page1Call = mockSuccessfulCall(
            4,
            0,
            listOf(
                PlaceDto("id1", "name1", CoordinatesDto(1.0, 1.0), LifespanDto("1991")),
                PlaceDto("id2", "name2", CoordinatesDto(2.0, 2.0), LifespanDto("1992"))
            )
        )
        val page2Call = mockErrorCall()
        val musicBrainz: MusicBrainzInterface = mock {
            on { getPlaces(anyString(), eq(limit), eq(0)) } doReturn page1Call
            on { getPlaces(anyString(), eq(limit), eq(2)) } doReturn page2Call
        }
        val placesProvider = NetworkPlacesProvider(limit, musicBrainz)
        val testObserver = TestObserver<List<Place>>()

        // when
        placesProvider.getPlaces("studios").subscribe(testObserver)

        // then
        testObserver.assertNoValues()
        testObserver.assertError(HttpException::class.java)
    }

    private fun mockSuccessfulCall(count: Int, offset: Int, places: List<PlaceDto>): Call<PlacesResponse> = mock {
        on { execute() } doReturn Response.success(PlacesResponse(count, offset, places))
    }

    private fun mockErrorCall(): Call<PlacesResponse> = mock {
        on { execute() } doReturn Response.error(500, ResponseBody.create(null, "Internal Server Error"))
    }
}