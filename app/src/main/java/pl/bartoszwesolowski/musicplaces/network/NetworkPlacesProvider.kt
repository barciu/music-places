package pl.bartoszwesolowski.musicplaces.network

import io.reactivex.Single
import pl.bartoszwesolowski.musicplaces.logic.PlacesProvider
import pl.bartoszwesolowski.musicplaces.model.Place
import pl.bartoszwesolowski.musicplaces.network.dto.PlaceDto
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class NetworkPlacesProvider(
    private val limit: Int,
    private val musicBrainz: MusicBrainzInterface = Retrofit.Builder()
        .baseUrl("https://musicbrainz.org")
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(MusicBrainzInterface::class.java)
) : PlacesProvider {

    override fun getPlaces(query: String): Single<List<Place>> {
        return Single.fromCallable {
            val places = mutableListOf<Place>()
            var offset = 0
            do {
                val response = musicBrainz.getPlaces("$query AND begin:{1991 TO *}", limit, offset).execute()
                if (!response.isSuccessful) {
                    throw HttpException(response)
                }
                places.addAll(response.body()!!.places.filter { it.coordinates != null }.map { it.toPlace() })
                offset += limit
            } while (response.body()!!.offset + response.body()!!.places.size < response.body()!!.count)
            places
        }
    }

    private fun PlaceDto.toPlace() = Place(
        id,
        name,
        coordinates!!.latitude,
        coordinates.longitude,
        lifespan.begin.substring(0, 4).toInt()
    )
}