package pl.bartoszwesolowski.musicplaces.network.dto

data class LifespanDto(val begin: String)