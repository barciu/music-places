package pl.bartoszwesolowski.musicplaces.network.dto

data class PlacesResponse(
    val count: Int,
    val offset: Int,
    val places: List<PlaceDto>
)