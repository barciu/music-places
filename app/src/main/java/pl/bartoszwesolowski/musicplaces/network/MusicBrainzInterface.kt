package pl.bartoszwesolowski.musicplaces.network

import pl.bartoszwesolowski.musicplaces.network.dto.PlacesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MusicBrainzInterface {

    @GET("ws/2/place?fmt=json")
    fun getPlaces(
        @Query("query") query: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Call<PlacesResponse>
}