package pl.bartoszwesolowski.musicplaces.network.dto

import com.squareup.moshi.Json

data class PlaceDto(
    val id: String,
    val name: String,
    val coordinates: CoordinatesDto?,
    @field:Json(name = "life-span")
    val lifespan: LifespanDto
)