package pl.bartoszwesolowski.musicplaces.network.dto

data class CoordinatesDto(
    val latitude: Double,
    val longitude: Double
)