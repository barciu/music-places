package pl.bartoszwesolowski.musicplaces.logic

import pl.bartoszwesolowski.musicplaces.model.Place

sealed class MapViewState {
    object MapNotReady : MapViewState()
    object NoQuery : MapViewState()
    data class InProgress(override val query: String) : MapViewState(), HasQuery
    data class NoResults(override val query: String) : MapViewState(), HasQuery
    data class ShowPlaces(
        override val query: String,
        val places: List<Place>,
        val animateCamera: Boolean
    ) : MapViewState(), HasQuery

    data class PlacesFetchingError(override val query: String) : MapViewState(), HasQuery
}

interface HasQuery {
    val query: String
}