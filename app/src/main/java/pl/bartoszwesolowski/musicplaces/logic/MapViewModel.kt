package pl.bartoszwesolowski.musicplaces.logic

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.bartoszwesolowski.musicplaces.model.Place
import pl.bartoszwesolowski.musicplaces.network.NetworkPlacesProvider
import java.util.concurrent.TimeUnit

class MapViewModel(
    private val placesProvider: PlacesProvider = NetworkPlacesProvider(limit = 20),
    private val ioScheduler: Scheduler = Schedulers.io(),
    private val mainThreadScheduler: Scheduler = AndroidSchedulers.mainThread(),
    private val intervalScheduler: Scheduler = Schedulers.computation()
) : ViewModel() {

    private val mapReadySubject = BehaviorSubject.createDefault(false)
    var mapReady: Boolean
        get() = mapReadySubject.value!!
        set(value) = mapReadySubject.onNext(value)

    private val placesSubject = BehaviorSubject.createDefault<PlacesState>(PlacesState.NoQuery)

    private val elapsedTimeSubject = BehaviorSubject.createDefault(0L)
    private var elapsedTimeDisposable: Disposable? = null

    val viewState: Observable<MapViewState> =
        Observable.combineLatest(
            mapReadySubject,
            placesSubject,
            elapsedTimeSubject.distinctUntilChanged(),
            Function3 { mapReady: Boolean, placesState: PlacesState, elapsedTime: Long ->
                if (!mapReady) {
                    MapViewState.MapNotReady
                } else {
                    when (placesState) {
                        is PlacesState.NoQuery -> MapViewState.NoQuery
                        is PlacesState.InProgress -> MapViewState.InProgress(placesState.query)
                        is PlacesState.NoResults -> MapViewState.NoResults(placesState.query)
                        is PlacesState.PlacesFetched -> {
                            val filteredPlaces = placesState.places.filterByTime(elapsedTime)
                            if (filteredPlaces.isEmpty()) {
                                elapsedTimeDisposable?.dispose()
                            }
                            val animateCamera = elapsedTime == 0L
                            MapViewState.ShowPlaces(placesState.query, filteredPlaces, animateCamera)
                        }
                        is PlacesState.PlacesFetchingError -> MapViewState.PlacesFetchingError(placesState.query)
                    }
                }
            }).distinctUntilChanged()

    private var placesDisposable: Disposable? = null

    fun findPlaces(query: String) {
        placesDisposable?.dispose()
        elapsedTimeDisposable?.dispose()
        placesSubject.onNext(PlacesState.InProgress(query))
        elapsedTimeSubject.onNext(0)
        placesDisposable = placesProvider.getPlaces(query)
            .subscribeOn(ioScheduler)
            .observeOn(mainThreadScheduler)
            .subscribe(
                { places ->
                    if (places.isEmpty()) {
                        placesSubject.onNext(PlacesState.NoResults(query))
                    } else {
                        placesSubject.onNext(PlacesState.PlacesFetched(query, places))
                        elapsedTimeDisposable = Observable.interval(0, 1, TimeUnit.SECONDS, intervalScheduler)
                            .observeOn(mainThreadScheduler)
                            .subscribe { elapsedTimeSubject.onNext(it) }
                    }
                },
                { placesSubject.onNext(PlacesState.PlacesFetchingError(query)) }
            )
    }

    private fun List<Place>.filterByTime(elapsedTime: Long) = filter { it.openYear - 1990 > elapsedTime }

    override fun onCleared() {
        placesDisposable?.dispose()
        elapsedTimeDisposable?.dispose()
    }
}