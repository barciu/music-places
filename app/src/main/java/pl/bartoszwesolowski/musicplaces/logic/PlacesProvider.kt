package pl.bartoszwesolowski.musicplaces.logic

import io.reactivex.Single
import pl.bartoszwesolowski.musicplaces.model.Place

interface PlacesProvider {

    fun getPlaces(query: String): Single<List<Place>>
}