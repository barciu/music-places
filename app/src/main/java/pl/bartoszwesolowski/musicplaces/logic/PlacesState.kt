package pl.bartoszwesolowski.musicplaces.logic

import pl.bartoszwesolowski.musicplaces.model.Place

sealed class PlacesState {
    object NoQuery : PlacesState()
    data class NoResults(val query: String) : PlacesState()
    data class InProgress(val query: String) : PlacesState()
    data class PlacesFetched(val query: String, val places: List<Place>) : PlacesState()
    data class PlacesFetchingError(val query: String) : PlacesState()
}