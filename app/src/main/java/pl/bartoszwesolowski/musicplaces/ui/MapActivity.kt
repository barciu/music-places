package pl.bartoszwesolowski.musicplaces.ui

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_map.*
import pl.bartoszwesolowski.musicplaces.R
import pl.bartoszwesolowski.musicplaces.logic.HasQuery
import pl.bartoszwesolowski.musicplaces.logic.MapViewModel
import pl.bartoszwesolowski.musicplaces.logic.MapViewState
import pl.bartoszwesolowski.musicplaces.model.Place

class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private lateinit var viewModel: MapViewModel
    private lateinit var viewStateDisposable: Disposable
    private var snackbar: Snackbar? = null
    private val markersByPlaceId = mutableMapOf<String, Marker>()
    private var state: MapViewState? = null
    private var cameraLatLngToRestore: LatLng? = null
    private var cameraZoomToRestore: Float? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        viewModel = ViewModelProviders.of(this).get(MapViewModel::class.java)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        cameraLatLngToRestore = savedInstanceState?.getParcelable(KEY_CAMERA_LAT_LNG)
        cameraZoomToRestore = savedInstanceState?.getFloat(KEY_CAMERA_LAT_LNG)

        viewStateDisposable = viewModel.viewState.subscribe { state ->
            this@MapActivity.state = state
            when (state) {
                is MapViewState.ShowPlaces -> {
                    progressBar.visibility = View.GONE
                    hideSnackbar()
                    showMarkers(state.places)
                    if (state.animateCamera) {
                        animateCamera(state.places)
                    }
                }
                is MapViewState.InProgress -> {
                    progressBar.visibility = View.VISIBLE
                    hideSnackbar()
                    map.clear()
                }
                is MapViewState.NoResults -> {
                    progressBar.visibility = View.GONE
                    showSnackbar(R.string.no_places_found)
                }
                is MapViewState.PlacesFetchingError -> {
                    progressBar.visibility = View.GONE
                    showSnackbar(R.string.fetching_places_failed)
                }
            }
        }
    }

    private fun showMarkers(places: List<Place>) {
        removeOutdatedMarkers(places)
        addMarkers(places)
    }

    private fun removeOutdatedMarkers(places: List<Place>) {
        val placeIds = places.map { it.id }.toSet()
        val removedPlaceIds = mutableListOf<String>()
        for (placeId in markersByPlaceId.keys) {
            if (!placeIds.contains(placeId)) {
                markersByPlaceId[placeId]!!.remove()
                removedPlaceIds.add(placeId)
            }
        }
        for (placeId in removedPlaceIds) {
            markersByPlaceId.remove(placeId)
        }
    }

    private fun addMarkers(places: List<Place>) {
        for (place in places) {
            if (markersByPlaceId.containsKey(place.id)) continue

            markersByPlaceId[place.id] = map.addMarker(
                MarkerOptions()
                    .position(
                        LatLng(
                            place.latitude,
                            place.longitude
                        )
                    )
                    .title("${place.name} (${place.openYear})")
            )
        }
    }

    private fun animateCamera(places: List<Place>) {
        val builder = LatLngBounds.builder()
        for (place in places) {
            builder.include(LatLng(place.latitude, place.longitude))
        }
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 64))
    }

    private fun showSnackbar(@StringRes resId: Int) {
        hideSnackbar()
        snackbar = Snackbar.make(
            findViewById(android.R.id.content),
            resId,
            Snackbar.LENGTH_INDEFINITE
        ).also { it.show() }
    }

    private fun hideSnackbar() {
        snackbar?.dismiss()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val query = intent?.getStringExtra(SearchManager.QUERY)
        if (query.isNullOrEmpty()) return
        viewModel.findPlaces(query)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.map_menu, menu)
        val searchItem = menu.findItem(R.id.search)
        (searchItem.actionView as SearchView).apply {
            val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
            setSearchableInfo(searchManager.getSearchableInfo(componentName))

            (state as? HasQuery)?.let { stateWithQuery ->
                searchItem.expandActionView()
                setQuery(stateWithQuery.query, false)
                clearFocus()
            }
        }
        return true
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        val latLng = cameraLatLngToRestore
        val zoom = cameraZoomToRestore
        if (latLng != null && zoom != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
        }
        viewModel.mapReady = true
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState?.putParcelable(KEY_CAMERA_LAT_LNG, map.cameraPosition.target)
        outState?.putFloat(KEY_CAMERA_ZOOM, map.cameraPosition.zoom)
    }

    override fun onDestroy() {
        viewStateDisposable.dispose()
        viewModel.mapReady = false
        super.onDestroy()
    }

    companion object {
        private const val KEY_CAMERA_LAT_LNG = "cameraLatLng"
        private const val KEY_CAMERA_ZOOM = "cameraZoom"
    }
}
